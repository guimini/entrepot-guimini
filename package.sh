#!/usr/bin/env bash
SCRIPT_URL="https://gitlab.com/djammadev/scripts/-/raw/master/package.sh"
function runScript() {
    curl -s $SCRIPT_URL | bash -s -- $@
}

runScript $@