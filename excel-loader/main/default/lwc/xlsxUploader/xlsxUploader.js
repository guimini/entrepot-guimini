import { LightningElement, wire, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { loadScript } from 'lightning/platformResourceLoader';
import getMapping from '@salesforce/apex/XLXSUploaderController.getMapping';
import * as xlsxUtils from 'c/xlsxUtils'
import xlsx from '@salesforce/resourceUrl/xlsx';

export default class XlsxUploader extends LightningElement {
    xlsxInitialized = false;
    sheetNames = [];
    selectedSheetName;
    @track mapping;
    @wire(getMapping)
    wiredMapping({ error, data }) {
        if (data) {
            this.mapping = data;
        } else if (error) {
            this.mapping = {};
        }
    }

    renderedCallback() {
        if (this.xlsxInitialized) {
            return;
        }
        this.xlsxInitialized = true;
        Promise.all([
            loadScript(this, xlsx + '/xlsx.full.min.js')
        ]).then(() => {
            this.initializeXlsx();
        })
            .catch(error => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Error loading XLSX',
                        message: error.message,
                        variant: 'error',
                    }),
                );
                this.xlsxInitialized = false;
            });
    }

    onFileChanged(event) {
        let files = event.srcElement.files;
        this.sheetNames = null;
        this.selectedSheetName = "";
        //this.onSheetSelected("");
        for (let file of files) {
            xlsxUtils.readFile(file, (response) => {
                this.sheets = response;
                this.sheetNames = [];
                for (let name in response) {
                    if (response.hasOwnProperty(name)) {
                        this.sheetNames.push(name);
                    }
                }
                console.log('this.sheets', this.sheets);
            }, this.mapping);
        }
    }

    initializeXlsx() {
        console.log('initializeXlsx', XLSX);
    }
}