/* eslint-disable no-undef */
export function readFile(file, callback, mapping) {
    if (file) {
        let filename = file.name.toLowerCase();
        let reader = new FileReader();
        reader.addEventListener('load', (event) => {
            let result = event.target.result;
            let response;
            if (filename.endsWith(".csv")) {
                response = parseCSV(result, mapping);
            } else {
                response = parseExcel(result, mapping);
            }
            if (callback) {
                callback(response, file);
            }
        }, false);
        if (filename.endsWith(".csv")) {
            reader.readAsText(file, "UTF-8");
        } else {
            reader.readAsBinaryString(file);
        }
    }
}

export function parseExcel(result, mapping) {
    let workbook = XLSX.read(result, {
        type: 'binary'
    });
    let response = {};
    workbook.SheetNames.forEach((sheetName) => {
        let sheetToJson = XLSX.utils.sheet_to_json(workbook.Sheets[sheetName], { header: 1 });
        let headers = sheetToJson[0];
        response[sheetName] = buildDataArray(headers, sheetToJson.splice(1), mapping);
    });
    return response;
}

export function parseCSV(result, mapping) {
    let split = result.split("\n");
    let lines = split.splice(1).filter((line) => {
        return line.trim() !== ''
    })
    let fields = splitCSV(split[0]);
    let data = buildCSVDataArray(fields, lines, mapping);
    return { "CSV": data };
}

export function splitCSV(line) {
    return line.split(/[;,\t]/);
}

export function buildCSVDataArray(fields, lines, mapping) {
    let array = lines.map((line) => {
        return splitCSV(line);
    });
    return buildDataArray(fields, array, mapping);
}

export function buildDataArray(headers, array, mapping) {
    return array.map((item) => {
        let data = {};
        for (let i = 0; i < headers.length && i < item.length; i++) {
            let header = headers[i];
            let itemValue = item[i];
            let dataKey = (mapping || {}).hasOwnProperty(header) ? (mapping || {})[header] : header;
            data[dataKey] = itemValue;
        }
        return data;
    });
}