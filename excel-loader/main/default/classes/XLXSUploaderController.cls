/**************************************************************************************
-- - Author        : Cheick Sissoko - Guimini
-- - Description   : Controller for XLSXUploader
--
-- Maintenance History: 
--
-- Date         Name  Version  Remarks 
-- -----------  ----  -------  -------------------------------------------------------
-- 27-JAN-2020  CMS    1.0     Initial version 
--------------------------------------------------------------------------------------
**************************************************************************************/
public with sharing class XLXSUploaderController {
    
    @AuraEnabled(cacheable = true)
    public static Map<String, String> getMapping() {
        Map<String, String> mapping = new Map<String, String>();
        mapping.put('Id', 'myId');
        return mapping;
    }
}